// HW ADVANCHED JS
//  Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
// в: Робота з зовнішніми ресурсами, Робота з функціями, які можуть генерувати помилки, Використання асинхронних операцій,Обробка неочікуваних помилок,
const rootElement = document.getElementById('root')
const ulElemet = document.createElement('ul')

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

  function isValidBook(book) {
    try {
        if (!('author' in book)) {
            throw new Error('Нема властивості "Автор"');
        }

        if (!('name' in book)) {
            throw new Error('Нема властивості "name"');
        }

        if (!('price' in book)) {
            throw new Error('Нема властивості "price"');
        }
        return true;
    } catch (error) {
        console.log(`Помилка перевірки книги: ${error.message}`);
        return false;
    }
}


  const validBooks = books.filter(book => isValidBook(book));

  console.log(validBooks);
  
  validBooks.forEach(book => {
      const item = document.createElement('li');
      item.textContent = `Автор: ${book.author ? book.author + ' - ' : ''}${book.name} (ціна ${book.price})`;
      ulElemet.appendChild(item);
  });
  
  rootElement.appendChild(ulElemet);